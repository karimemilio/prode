DROP TABLE  "polls_prediction"

CREATE TABLE
  "polls_prediction" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "home_score" integer NOT NULL,
    "away_score" integer NOT NULL,
    "game_id" integer NOT NULL,
    "user_id" integer NOT NULL
  )