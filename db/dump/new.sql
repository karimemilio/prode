CREATE TABLE
  "polls_new" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "image"  varchar(255) NULL,
    "title"  varchar(255) NULL,
    "description"  varchar(255) NULL,
    "active" boolean
  )