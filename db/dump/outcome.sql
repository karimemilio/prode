DROP TABLE "polls_resultados"

CREATE TABLE
  "polls_outcome" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "home_score" integer NULL,
    "away_score" integer NULL
  )