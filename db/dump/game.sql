DROP TABLE "polls_partidos"

CREATE TABLE
  "polls_game" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "group_id" integer NOT NULL,
    "stage_id" integer NOT NULL,
    "round" integer NOT NULL,
    "home_id" integer NOT NULL,
    "away_id" integer NOT NULL,
    "date" date NOT NULL,
    "outcome_id" integer NULL
  )

insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (1, 1, 1, 1, 3, 4, '2022-11-21', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (2, 1, 1, 1, 1, 2, '2022-11-25', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (3, 1, 1, 2, 1, 3, '2022-11-21', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (4, 1, 1, 2, 4, 2, '2022-11-25', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (5, 1, 1, 3, 4, 1, '2022-11-29', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (6, 1, 1, 3, 2, 3, '2022-11-29', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (7, 2, 1, 1, 5, 6, '2022-11-21', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (8, 2, 1, 1, 7, 8, '2022-11-21', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (9, 2, 1, 2, 8, 6, '2022-11-25', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (10, 2, 1, 2, 5, 7, '2022-11-25', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (11, 2, 1, 3, 8, 5, '2022-11-29', null);
insert into `polls_game` (`id`, `group_id`, `stage_id`, `round`, `home_id`, `away_id`, `date`, `outcome_id`) values (12, 2, 1, 3, 6, 7, '2022-11-29', null);