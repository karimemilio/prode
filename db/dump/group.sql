DROP TABLE "polls_grupos" 

CREATE TABLE "polls_group" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" char NOT NULL,
    "description" varchar(50) NULL
  )
  
insert into `polls_group` (`description`, `name`) values ('Grupo C', 'C');
insert into `polls_group` (`description`, `name`) values ('Grupo D', 'D');
insert into `polls_group` (`description`, `name`) values ('Grupo B', 'B');
insert into `polls_group` (`description`, `name`) values ('Grupo E', 'E');
insert into `polls_group` (`description`, `name`) values ('Grupo F', 'F');
insert into `polls_group` (`description`, `name`) values ('Grupo G', 'G');
insert into `polls_group` (`description`, `name`) values ('Grupo H', 'H');
insert into `polls_group` (`description`, `name`) values ('Grupo A', 'A');