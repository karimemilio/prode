DROP TABLE "polls_paises"

CREATE TABLE
  "polls_country" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" varchar(35) NOT NULL,
    "description" varchar(50) NULL,
    "group_id" integer NOT null,
    "flag" varchar(255) NULL
  )

insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('País anfitrión', 'https://www.promiedos.com.ar/images/ps/qatar.gif', 1, 'Qatar');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/ecuador.gif', 1, 'Ecuador');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/senegal.gif', 1, 'Senegal');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/paisesbajos.gif', 1, 'Países Bajos');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/inglaterra.gif', 2, 'Inglaterra');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/iran.gif', 2, 'Iran');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/usa.gif', 2, 'USA');
insert into `polls_country` (`description`, `flag`, `group_id`, `name`) values ('-', 'https://www.promiedos.com.ar/images/ps/gales.gif', 2, 'Gales');