from django.core.mail import send_mail
from django.http import HttpResponse
from django.template import loader

from .models import Country, Group, Game, Outcome, Prediction, New
from django.contrib.auth.decorators import login_required

from django.shortcuts import render
from .forms import InputForm
from datetime import date
from itertools import zip_longest

from django.contrib.auth.models import User

def soon(request):
    """Vista correspondiente a la pantalla "Próximamente" de la aplicación, la cual anuncia que el sitio está en desarrollo"""
    template = loader.get_template('polls/soon.html')
    context = {
    }
    return HttpResponse(template.render(context, request))

def base(request):
    """Vista correspondiente a la base de la aplicación, con la barra de menú lateral, estilos y configuraciones"""
    template = loader.get_template('polls/base.html')
    context = {
    }
    return HttpResponse(template.render(context, request))

def ger_fecha_actual():
    """Función que calcula devuelve la próxima fecha a disputar que aún no haya comenzado

    Returns:
        integer: Número de la próxima fecha a disputarse
    """
    # Determino la fecha actual buscando a que fecha corresponde el próximo partido comparándolo con la fecha de hoy.
    fecha_actual = Game.objects.filter(date = date.today()).values('date').first()
    if (fecha_actual):
        fecha_actual = fecha_actual['nro_fecha']
        print(f"La próxima fecha es la número {fecha_actual}")
    else:
        fecha_actual = 1
        print(f"El mundial no comenzó. La próxima fecha es la número {fecha_actual}")
    return fecha_actual

@login_required
def home(request):
    """Vista correspondiente a la pantalla principal (Inicio/Home) de la aplicación.

    Returns:
        HttpResponse: Devuelve el template correspondiente. Además manda la próxima fecha a disputar y las noticias a renderizarse en el carrousel
    """
    fecha_actual = ger_fecha_actual()
    noticias = New.objects.filter(active = True) #Filtra las noticias activas para el carrousel
    template = loader.get_template('polls/home.html')
    context = {
        'fecha_actual': fecha_actual,
        'noticias': noticias
    }
    return HttpResponse(template.render(context, request))


@login_required
def paises(request):
    """Vista correspondiente al listado de países.

    Returns:
        HttpResponse: Devuelve el template correspondiente, enviando todos los países obtenidos de la Base de datos ordenados alfabéticamente de forma ascendente.
    """
    paises = Country.objects.order_by('name') #Filtra los países por orden alfabético
    template = loader.get_template('polls/paises.html')
    context = {
        'paises': paises,
    }
    return HttpResponse(template.render(context, request))

def renderTable(group):
    """Función que crea la tabla de posiciones de un grupo específico recibido por parámetro.

    Args:
        group (integer): Recibe un id de un grupo para el cual real

    Returns:
        List: Devuelve una lista de diccionarios, ordenada por Puntos obtenidos en primer lugar y Diferencia de Gol en segundo lugar.
        Cada diccionario corresponde a un equipo y sus PUNTOS, PARTIDOS GANADOS, PARTIDOS EMPATADOS, PARTIDOS PERDIDOS, PARTIDOS JUGADOS,
        GOLES A FAVOR, GOLES EN CONTRA Y DIFERENCIA DE GOL. Además se arega el ID del equipo para identificarlo.
    """
    tabla_grupo = {}
    partidos = Game.objects.filter(group_id=group)
    for partido in partidos:
        outcome = Outcome.objects.filter(id=partido.outcome_id).first()
        if outcome:
            try:
                tabla_grupo[partido.home_id]["GF"]=tabla_grupo[partido.home_id]["GF"]+outcome.home_score
                tabla_grupo[partido.home_id]["GC"]=tabla_grupo[partido.home_id]["GC"]+outcome.away_score
                if outcome.home_score>outcome.away_score:
                    tabla_grupo[partido.home_id]["PTS"]=tabla_grupo[partido.home_id]["PTS"]+3
                    tabla_grupo[partido.home_id]["PG"]=tabla_grupo[partido.home_id]["PG"]+1
                elif outcome.home_score==outcome.away_score:
                    tabla_grupo[partido.home_id]["PTS"]=tabla_grupo[partido.home_id]["PTS"]+1
                    tabla_grupo[partido.home_id]["PE"]=tabla_grupo[partido.home_id]["PE"]+1
                else:
                    tabla_grupo[partido.home_id]["PP"]=tabla_grupo[partido.home_id]["PP"]+1
            except:
                tabla_grupo[partido.home_id]={}
                if outcome.home_score>outcome.away_score:
                    tabla_grupo[partido.home_id]["PTS"]=3
                    tabla_grupo[partido.home_id]["PG"]=1
                    tabla_grupo[partido.home_id]["PE"]=0
                    tabla_grupo[partido.home_id]["PP"]=0
                elif outcome.home_score<outcome.away_score:
                    tabla_grupo[partido.home_id]["PTS"]=0
                    tabla_grupo[partido.home_id]["PG"]=0
                    tabla_grupo[partido.home_id]["PE"]=0
                    tabla_grupo[partido.home_id]["PP"]=1
                else:
                    tabla_grupo[partido.home_id]["PTS"]=1
                    tabla_grupo[partido.home_id]["PG"]=0
                    tabla_grupo[partido.home_id]["PE"]=1
                    tabla_grupo[partido.home_id]["PP"]=0
                tabla_grupo[partido.home_id]["GF"]=outcome.home_score
                tabla_grupo[partido.home_id]["GC"]=outcome.away_score
            try:
                tabla_grupo[partido.away_id]["GF"]=tabla_grupo[partido.away_id]["GF"]+outcome.away_score
                tabla_grupo[partido.away_id]["GC"]=tabla_grupo[partido.away_id]["GC"]+outcome.home_score
                if outcome.home_score<outcome.away_score:
                    tabla_grupo[partido.away_id]["PTS"]=tabla_grupo[partido.away_id]["PTS"]+3
                    tabla_grupo[partido.away_id]["PG"]=tabla_grupo[partido.away_id]["PG"]+1
                elif outcome.home_score==outcome.away_score:
                    tabla_grupo[partido.away_id]["PTS"]=tabla_grupo[partido.away_id]["PTS"]+1
                    tabla_grupo[partido.away_id]["PE"]=tabla_grupo[partido.away_id]["PE"]+1
                else:
                    tabla_grupo[partido.away_id]["PP"]=tabla_grupo[partido.away_id]["PP"]+1
            except:
                tabla_grupo[partido.away_id]={}
                if outcome.home_score<outcome.away_score:
                    tabla_grupo[partido.away_id]["PTS"]=3
                    tabla_grupo[partido.away_id]["PG"]=1
                    tabla_grupo[partido.away_id]["PE"]=0
                    tabla_grupo[partido.away_id]["PP"]=0
                elif outcome.home_score>outcome.away_score:
                    tabla_grupo[partido.away_id]["PTS"]=0
                    tabla_grupo[partido.away_id]["PG"]=0
                    tabla_grupo[partido.away_id]["PE"]=0
                    tabla_grupo[partido.away_id]["PP"]=1
                else:
                    tabla_grupo[partido.away_id]["PTS"]=1
                    tabla_grupo[partido.away_id]["PG"]=0
                    tabla_grupo[partido.away_id]["PE"]=1
                    tabla_grupo[partido.away_id]["PP"]=0
                tabla_grupo[partido.away_id]["GF"]=outcome.away_score
                tabla_grupo[partido.away_id]["GC"]=outcome.home_score
    for team in tabla_grupo:
            tabla_grupo[team]["PJ"]=tabla_grupo[team]["PG"]+tabla_grupo[team]["PE"]+tabla_grupo[team]["PP"]
            tabla_grupo[team]["DG"]=tabla_grupo[team]["GF"]-tabla_grupo[team]["GC"]
    if not tabla_grupo:
        paises = Country.objects.values("id").filter(group_id=group)
        for pais in paises:
            aux=pais["id"]
            tabla_grupo[aux]={}
            tabla_grupo[aux]["PTS"]=0
            tabla_grupo[aux]["PJ"]=0
            tabla_grupo[aux]["PG"]=0
            tabla_grupo[aux]["PE"]=0
            tabla_grupo[aux]["PP"]=0
            tabla_grupo[aux]["GF"]=0
            tabla_grupo[aux]["GC"]=0
            tabla_grupo[aux]["DG"]=0

    aux = []
    for each in tabla_grupo:
        tabla_grupo[each]["EQ"]=(Country.objects.values("name").filter(id=each).first())["name"]
        aux.append(tabla_grupo[each])
    return sorted(aux, key=lambda d: (d['PTS'], d['DG']), reverse=True)

@login_required
def grupos(request):
    """Vista correspondiente a las tablas de posiciones de todos los grupos.

    Returns:
        HttpResponse: Devuelve el template correspondiente, enviando todos los grupos obtenidos de la Base de datos,
        creando las respectivas tablas y ordenándolas alfabéticamente de forma ascendente.
    """
    a=renderTable(1)
    b=renderTable(2)
    c=renderTable(3)
    d=renderTable(4)
    e=renderTable(5)
    f=renderTable(6)
    g=renderTable(7)
    h=renderTable(8)

    paises = Country.objects.order_by('name')
    grupos = Group.objects.order_by('name')
    template = loader.get_template('polls/grupos.html')
    context = {
        'a': a,
        'b': b,
        'c': c,
        'd': d,
        'e': e,
        'f': f,
        'g': g,
        'h': h,
        'grupos': grupos,
    }
    return HttpResponse(template.render(context, request))

@login_required
def predicciones(request):
    fecha_actual = ger_fecha_actual()
    predicciones = Prediction.objects.filter(user_id=request.user).order_by('-game__date')
    fechas = [1,2,3,4,5,6]
    template = loader.get_template('polls/predicciones.html')
    context = {
        'predicciones': predicciones,
        'fechas': fechas,
        'fecha_actual': fecha_actual,
    }
    return HttpResponse(template.render(context, request))

# def respue(request):
#     if request.method == 'POST':

#         print("Post realizado!")

#         original_request=request.POST

#         copied_request=original_request.copy()
#         fecha = copied_request.pop("fecha")
#         token = copied_request.pop("csrfmiddlewaretoken")

#         cant_partidos=len(fecha)
#         fecha=fecha[0]

#         print(f"Se cargaron {cant_partidos} partidos de la fecha {fecha}")

#         for each in copied_request:
#             print(f"El equipo numero {each} metió {copied_request[each]} goles")
#         # data = original_request.get("data", "0")
#         # name = original_request.get("name", "0")

#         # aux = form.cleaned_data['local']
#         print(copied_request)
#         cant_partidos = type(request.POST)

#         print(cant_partidos)
#         partidos = Game.objects.order_by('id').filter(round="1")
#         paises = Country.objects.order_by('-name')
#         template = loader.get_template('polls/carga.html')
#         context = {
#             'paises': paises,
#         }
#         context['form']= InputForm()
#     return HttpResponse(template.render(context, request))

@login_required
def cargar(request, nro_fecha):

    # Para el post
    if request.method == 'POST':
        print("Post realizado!")

        original_request=request.POST

        copied_request=original_request.copy()

        fecha = copied_request.pop("fecha")
        token = copied_request.pop("csrfmiddlewaretoken")

        cant_partidos=len(fecha)
        nro_partidos=copied_request.keys()
        fecha=fecha[0]

        print(f"Se cargaron {cant_partidos} partidos de la fecha {fecha}")
        print("Se predijeron los partidos con id:")
        for each in nro_partidos:
            print(f"-{each}")

        for each in nro_partidos:
            print(f"El partido numero {each} se predijo {copied_request.getlist(each)[0]} a {copied_request.getlist(each)[1]}")
            prediccion = Prediction.objects.filter(user_id=request.user.id, game_id=each).first()
            print(f"LA PREDDICCION ES {prediccion}")
            if prediccion != None:
                # EDITAR porque game ya tiene un campo "outcome_id" diferente de null
                #TODO: FILTRAR USUARIO usuario_id=request.user.id
                Prediction.objects.filter(user_id=request.user.id, game_id=each).update(home_score=copied_request.getlist(each)[0])
                Prediction.objects.filter(user_id=request.user.id, game_id=each).update(away_score=copied_request.getlist(each)[0])
                print(f"Predicción del partido {each} editado con éxito")
            else:
                # crear
                nueva_prediccion = Prediction(home_score= copied_request.getlist(each)[0], away_score=copied_request.getlist(each)[1], game_id=each, user_id=request.user.id)
                # TODO: AGREGARLE A LOS RESULTADOS Y DEMAS TABLAS, CAMPOS DE AUDITORIA
                nueva_prediccion.save() #TODO: MEJORAR ESTO PORQUE EL SAVE DEBERIA GUARDAR TOODOS LOS RESULTS NO DE A UNO, POR SI FALLA LA TRANSACCION
                print(f"Resultado del partido {each} creado con éxito")

        print("Resultados guadados con éxito")
        # send_mail('test email', 'hello world', 'karimemilio98@gmail.com', ['karimemilio98@gmail.com'])

        return home(request)

    # Para el no post
    else:
        print(f"Se presenta para cargar la fecha número: {nro_fecha}")
        # resultados = Outcome.objects.order_by('id').filter(nro_fecha = nro_fecha)
        predicciones = Prediction.objects.filter(user_id=request.user.id, game__round=nro_fecha).order_by('id').values('game_id', 'home_score', 'away_score')
        # latest_question_list = Paises.objects.order_by('-name')[:5]
        partidos = Game.objects.order_by('id').filter(round=nro_fecha)
        print(predicciones)
        print(partidos)
        paises = Country.objects.order_by('id')
        fecha = Game.objects.values('date')[:1]
        # paises = Paises.objects.select_related('-name')
        # TODO: REDIRIGIR AL HOME O A LA SIGUIENTE FECHA. MANEJAR EL
        template = loader.get_template('polls/cargar.html')
        context = {
            'paises': paises,
            'partidos': zip_longest(partidos, predicciones),
            'fecha': nro_fecha,
            'fecha_sig': nro_fecha + 1,
            'fecha_ant': nro_fecha - 1,
        }
        context['form']= InputForm()
        return HttpResponse(template.render(context, request))

@login_required
def perfil(request):
    partidos = Game.objects.get(pk=1)
    predicciones = Prediction.objects.order_by('id')
    # paises = Country.objects.order_by('-name')
    try:
        print(predicciones)
    except:
        print("sad")
    for each in predicciones:
        print(each.home_score)
    template = loader.get_template('polls/perfil.html')
    context = {
        'resultados': predicciones,
    }
    return HttpResponse(template.render(context, request))

@login_required
def reglas(request):
    paises = Country.objects.order_by('-name')
    template = loader.get_template('polls/reglas.html')
    context = {
        'paises': paises,
    }
    return HttpResponse(template.render(context, request))

def getGanador(resultado):
    resul = str(resultado.home_score)+str(resultado.away_score)
    if (resultado.home_score == resultado.away_score):
        return "tie", resul
    elif (resultado.home_score>resultado.away_score):
        return "home", resul
    else:
        return "away", resul

@login_required
def posiciones(request):
    usuarios = User.objects.order_by('-username')
    tabla = []
    for usuario in usuarios:
        predicciones = Prediction.objects.filter(user_id=usuario.id)
        aux = {}
        aux["PTS"]=0
        aux["PG"]=0
        aux["PL"]=0
        aux["PJ"]=0
        aux["PP"]=0
        aux["usr"]=usuario.username
        for each in predicciones:
            game = Game.objects.filter(id=each.game_id).first()
            outcome = Outcome.objects.filter(id=game.outcome_id).first()
            if outcome:
                ganadorP, resultadoP = getGanador(each)
                ganador, resultado = getGanador(outcome)
                if (ganador == ganadorP):
                    if (resultado == resultadoP):
                        # Pleno
                        aux["PL"]=aux["PL"]+3
                    else:
                        # Ganado
                        aux["PG"]=aux["PG"]+1
                else:
                    # Perdido
                    aux["PP"]=aux["PP"]+1
                aux["PJ"]=aux["PJ"]+1
        aux["PTS"]=aux["PG"]+aux["PL"]*3
        tabla.append(aux)
    print(tabla)
    tabla=sorted(tabla, key=lambda d: (d["PL"], d['PG']), reverse=True)
    template = loader.get_template('polls/posiciones.html')
    context = {
        'usuarios': usuarios,
        'tabla': tabla,
    }
    return HttpResponse(template.render(context, request))