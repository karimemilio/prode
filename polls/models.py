import datetime

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.conf import settings
from django.contrib.auth.models import User

class Country(models.Model):

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=35)
    description = models.CharField(max_length=50)
    group = models.ForeignKey('Group', on_delete=models.CASCADE, related_name="group_id")
    flag = models.CharField(max_length=255)

    class Meta:
            managed = True
            db_table = 'polls_country'

    def __str__(self):
        return self.name

class Group(models.Model):

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1)
    
    class Meta:
            managed = True
            db_table = 'polls_group'

    def __str__(self):
        return self.name

class Stage(models.Model):

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)

    class Meta:
            managed = True
            db_table = 'polls_stage'

    def __str__(self):
        return self.name

class Game(models.Model):
    id = models.IntegerField(primary_key=True)
    group = models.ForeignKey('Group', on_delete=models.CASCADE, related_name="gruop_id")
    stage = models.ForeignKey('Stage', on_delete=models.CASCADE, related_name="stage_id")
    round = models.CharField(max_length=200)
    home = models.ForeignKey('Country', on_delete=models.CASCADE, related_name="home_id")
    away = models.ForeignKey('Country', on_delete=models.CASCADE, related_name="away_id")
    date = models.DateField()
    outcome = models.ForeignKey('Outcome', on_delete=models.CASCADE,  related_name="outcome_id")

    class Meta:
            managed = True
            db_table = 'polls_game'

    def __str__(self):
        return f'{self.home} - {self.away}'

class Outcome(models.Model):
    id = models.IntegerField(primary_key=True)
    home_score = models.IntegerField()
    away_score = models.IntegerField()
    # usuario = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name="usuario_id")

    class Meta:
            managed = True
            db_table = 'polls_outcome'

    def __str__(self):
        return f'{self.home_score} - {self.away_score}'

class Prediction(models.Model):
    id = models.IntegerField(primary_key=True)
    home_score = models.IntegerField()
    away_score = models.IntegerField()
    game = models.ForeignKey('Game', on_delete=models.CASCADE, related_name="game_id")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_id")

    class Meta:
            managed = True
            db_table = 'polls_prediction'

    def __str__(self):
        return f'{self.home_score} - {self.away_score}'

class New(models.Model):
    id = models.IntegerField(primary_key=True)
    image = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    active = models.BooleanField()
    link = models.CharField(max_length=255)

    class Meta:
        managed = True
        db_table = 'polls_new'

    def __str__(self):
        return f'{self.title} - {self.active}'
