from django.urls import path

from . import views

urlpatterns = [
    path('base', views.base, name='base'),
    path('cargar/<int:nro_fecha>/' , views.cargar, name= "cargar"),
    path('paises', views.paises, name='paises'),
    path('grupos', views.grupos, name='grupos'),
    path('', views.soon, name='soon'),
    path('soon', views.soon, name='soon'),
    path('home', views.home, name='home'),
    # path('respue', views.respue, name='respue'),
    path('predicciones', views.predicciones, name='predicciones'),
    path('perfil', views.perfil, name='perfil'),
    path('reglas', views.reglas, name='reglas'),
    path('posiciones', views.posiciones, name='posiciones'),
]