from django.contrib import admin

from .models import Country, Group, Game, Outcome, Stage, Prediction, New

admin.site.register(Country)
admin.site.register(Game)
admin.site.register(Group)
admin.site.register(New)
admin.site.register(Outcome)
admin.site.register(Prediction)
admin.site.register(Stage)